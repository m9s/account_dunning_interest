#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Account Dunning Interest',
    'name_de_DE': 'Buchhaltung Mahnwesen Verzugszinsen',
    'version': '2.2.0',
    'author': 'virtual-things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Adds interests to dunnings (aka reminder) for open items.
    ''',
    'description_de_DE': '''
    - Fügt Verzugszinsen zu Mahnungen hinzu
    ''',
    'depends': [
        'account_dunning',
    ],
    'xml': [
        'dunning.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
