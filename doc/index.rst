account_dunning_interest Module
###############################

This document describes the general concepts of the account_dunning_interest
module. All widgets in this module have a tool-tip help which is shown, when
the mouse is over a special widget.

The account_dunning_interest module provides additional late payment interests
for each dunning line.

Configuration
=============

The interest can be set in the dunning term, level:

 * Financial Management > Configuration > Dunning > Dunning Terms



Interest in Dunning Lines
=========================

The interests are evaluated for each dunning line in the actual level.

The dunning self holds a total of all line interests.

The base amount is the amount to pay of a line. The interest for a line is 
calcuulated like this:

base amount * interest(level) * 360 / 100

The interests are added to the total amount to pay.


