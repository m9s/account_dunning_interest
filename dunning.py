#This file is part of Tryton.  The COPYRIGHT file at the top level
#of this repository contains the full copyright notices and license terms.
'Dunning'
import datetime
import copy
import base64
from decimal import Decimal
from trytond.model import ModelWorkflow, ModelView, ModelSQL, fields
from trytond.report import Report
from trytond.wizard import Wizard
from trytond.pyson import Eval
from trytond.pool import Pool


ZERO = Decimal('0.0')

class DunningLevel(ModelSQL, ModelView):
    'Dunning Level'
    _name = 'account.dunning.level'
    _description = __doc__

    interest_percentage = fields.Numeric('Interest Percentage',
            digits=(16, 10),
            help='Add interest on late payments percentage for dunning lines '
            'in this level.')

    def default_interest_percentage(self):
        return ZERO

DunningLevel()


class Dunning(ModelSQL, ModelView):
    'Dunning'
    _name = 'account.dunning'

    interest = fields.Function(fields.Numeric('Interest',
            digits=(16, Eval('currency_digits', 2)), depends=['currency_digits'],
            help='The sum of all interests on late payments for this dunning'),
            'get_interest')

    def get_interest(self, ids, name):
        res = {}
        currency_obj = Pool().get('currency.currency')
        for dunning in self.browse(ids):
            interest =  ZERO
            for line in dunning.lines:
                if not line.block:
                    interest += line.interest
            res[dunning.id] = currency_obj.round(dunning.company.currency,
                    interest)
        return res

    def get_total(self, ids, name):
        currency_obj = Pool().get('currency.currency')
        charge =  ZERO
        res = super(Dunning, self).get_total(ids, name)
        for dunning in self.browse(ids):
            res[dunning.id] += currency_obj.round(dunning.company.currency,
                    dunning.interest)
        return res

    def _on_change_lines_payments(self, vals):
        total = vals.get('total', ZERO)
        interest = vals.get('interest', ZERO)
        lines = vals.get('lines', [])

        res = super(Dunning, self)._on_change_lines_payments(vals)

        res = res.copy()
        res.setdefault('interest', ZERO)
        for line in lines:
            line_interest = line.get('interest', ZERO)
            if not line.get('block'):
                res['interest'] += line_interest
                res['total'] += line_interest

        return res

Dunning()


class DunningLine(ModelSQL, ModelView):
    'Dunning Line'
    _name = 'account.dunning.line'

    interest = fields.Function(fields.Numeric('Interest',
            digits=(16, Eval('currency_digits', 2)), depends=['currency_digits'],
            help='Interest on late payments for this dunning line.'),
            'get_interest')

    def get_interest(self, ids, name):
        currency_obj = Pool().get('currency.currency')
        res = {}
        for line in self.browse(ids):
            interest = ZERO
            if 'level' in line \
                              and line.level \
                              and 'interest_percentage' in line.level:
                interest = line.amount \
                         * line.level.interest_percentage \
                         * line.overdue / 360 \
                         / 100
            res[line.id] = currency_obj.round(line.dunning.company.currency,
                    interest)
        return res

DunningLine()
